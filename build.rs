extern crate cc;


fn main() {
    // Build libfft.a using nvcc
    cc::Build::new()
        .cuda(true)
        .flag("-gencode").flag("arch=compute_52,code=sm_52")
        .file("src/fft.cu")
        .compile("libfft.a");

    // Build libreduce.a using nvcc
    cc::Build::new()
        .cuda(true)
        .flag("-gencode").flag("arch=compute_52,code=sm_52")
        .file("src/reduce.cu")
        .compile("libreduce.a");

    println!("cargo:rustc-link-search=native=/usr/local/cuda/lib64");
    println!("cargo:rustc-link-lib=cudart");
    println!("cargo:rustc-link-lib=cufft");
}
