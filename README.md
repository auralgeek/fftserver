FFT Server
==========

Simple service that will take in FFT requests from other client applications
via some form of IPC, perform a CUDA accelerated FFT, and hand back the result
data.

Installation
------------

REQUIRES that [CUDA](https://developer.nvidia.com/cuda-downloads) already be
installed on the system, and assumes the user is on an Ubuntu box.  It might
work in a different Linux environment, but I haven't tested with any but my
own.  Assumes Maxwell GPU architecture, but you can choose from several
different options as detailed
[here](https://github.com/alexcrichton/cc-rs#cuda-c-support).  Just modify the
`build.rs` file in the root directory.

Once CUDA is installed you can simply do a:

```sh
$ cargo build
```

as usual to build the library, and a:

```sh
$ cargo test -- --test-threads=1
```

to run the tests.  We run the tests with only 1 thread because CUDA freaks out
if you try to run different kernels in parallel.
