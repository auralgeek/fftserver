#include <stdio.h>


extern "C" {

__inline__ __device__
int warp_reduce_sum(int sum) {
  for (int i = 16; i > 0; i /= 2) {
    sum += __shfl_down_sync(0xffffffff, sum, i, 32);
  }
  return sum;
}

__inline__ __device__
int block_reduce_sum(int sum) {
  // Shared memory for 32 partial sums
  static __shared__ int shared[32];
  int lane = threadIdx.x % warpSize;
  int wid = threadIdx.x / warpSize;

  // Each warp performs partial reduction
  sum = warp_reduce_sum(sum);

  // Write reduced sum to shared memory
  if (lane == 0) {
    shared[wid] = sum;
  }

  // Wait for all partial reductions
  __syncthreads();

  // Read from shared memory only if that warp existed
  sum = (threadIdx.x < blockDim.x / warpSize) ? shared[lane] : 0;

  // Final reduce within first warp
  if (wid == 0) {
    sum = warp_reduce_sum(sum);
  }

  return sum;
}

__global__
void kernel_sum_reduce(int *input, int *output, size_t num_elements) {
  int sum = 0;

  // Reduce multiple elements per thread
  for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < num_elements; i += blockDim.x * gridDim.x) {
    sum += input[i];
  }

  sum = block_reduce_sum(sum);

  if (threadIdx.x == 0) {
    output[blockIdx.x] = sum;
  }
}

void reduce_sum(int *input, int *output, size_t num_elements) {
  int threads = 512;
  int blocks = min(((int)num_elements + threads - 1) / threads, 1024);

  kernel_sum_reduce<<<blocks, threads>>>(input, output, num_elements);
  kernel_sum_reduce<<<1, 1024>>>(output, output, blocks);
}

int cuda_sum(int *input, size_t num_elements) {

  int *d_input;
  cudaMalloc(&d_input, sizeof(int) * num_elements);
  cudaMemcpy(d_input, input, sizeof(int) * num_elements, cudaMemcpyHostToDevice);

  int *d_output;
  cudaMallocManaged(&d_output, sizeof(int));

  reduce_sum(d_input, d_output, num_elements);
  cudaDeviceSynchronize();

  int output = *d_output;
  cudaFree(d_input);
  cudaFree(d_output);

  return output;
}
}
