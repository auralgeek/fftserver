#include <stdio.h>
#include <cufft.h>


extern "C" {

int check_cuda() {
  if (cudaGetLastError() != cudaSuccess){
    fprintf(stderr, "Cuda error\n");
    return -1;
  }
  else return 0;
}

int check_cufft(int retval) {
  if (retval != CUFFT_SUCCESS){
    fprintf(stderr, "cufft error\n");
    return -1;
  }
  else return 0;
}

int cuda_fft(float *input, float *output, int num_elements) {

  // Copy data from caller, and pass to the device
  cufftComplex *d_input, *d_output;
  cudaMalloc(&d_input, sizeof(cufftComplex) * num_elements);
  cudaMalloc(&d_output, sizeof(cufftComplex) * num_elements);
  cudaMemcpy(d_input, input, sizeof(cufftComplex) * num_elements, cudaMemcpyHostToDevice);
  check_cuda();

  cufftHandle plan;
  check_cufft(cufftPlan1d(&plan, num_elements, CUFFT_C2C, 1));
  check_cufft(cufftExecC2C(plan, d_input, d_output, CUFFT_FORWARD));

  if (cudaDeviceSynchronize() != cudaSuccess){
    fprintf(stderr, "Cuda error: Failed to synchronize\n");
    return -1;
  }

  // Copy output from device to host, and pass back the data to caller
  cudaMemcpy(output, d_output, sizeof(cufftComplex) * num_elements, cudaMemcpyDeviceToHost);
  check_cuda();

  cufftDestroy(plan);
  cudaFree(d_input);
  cudaFree(d_output);

  return 0;
}

int cuda_fft_batch(float *input, float *output, int num_elements, int num_ffts) {

  // Copy data from caller, and pass to the device
  cufftComplex *d_input, *d_output;
  cudaMalloc(&d_input, sizeof(cufftComplex) * num_elements * num_ffts);
  cudaMalloc(&d_output, sizeof(cufftComplex) * num_elements * num_ffts);
  cudaMemcpy(d_input, input, sizeof(cufftComplex) * num_elements * num_ffts, cudaMemcpyHostToDevice);
  check_cuda();

  cufftHandle plan;
  check_cufft(cufftPlan1d(&plan, num_elements, CUFFT_C2C, num_ffts));
  check_cufft(cufftExecC2C(plan, d_input, d_output, CUFFT_FORWARD));

  if (cudaDeviceSynchronize() != cudaSuccess){
    fprintf(stderr, "Cuda error: Failed to synchronize\n");
    return -1;
  }

  // Copy output from device to host, and pass back the data to caller
  cudaMemcpy(output, d_output, sizeof(cufftComplex) * num_elements * num_ffts, cudaMemcpyDeviceToHost);
  check_cuda();

  cufftDestroy(plan);
  cudaFree(d_input);
  cudaFree(d_output);

  return 0;
}
}
