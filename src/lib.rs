//! Various CUDA accelerated FFT functions.
//!
//! Utilizes CUDA and the CUFFT library for FFTs on Nvidia GPUs.

extern crate num_complex;

use num_complex::Complex;

#[link(name = "fft", kind = "static")]
extern "C" {
    fn cuda_fft(input: *const Complex<f32>, output: *mut Complex<f32>, num_elements: usize);
    fn cuda_fft_batch(input: *const Complex<f32>, output: *mut Complex<f32>, num_elements: usize, num_ffts: usize);
}

#[link(name = "reduce", kind = "static")]
extern "C" {
    fn cuda_sum(input: *const i32, num_elements: usize) -> i32;
}

/// Sums input vector and returns result.
///
/// # Arguments
///
/// * `input` - Input vector to be summed.
///
/// # Examples
///
/// ```
/// use fftserver::sum;
///
/// let num_elements = 1 << 15;
/// let mut input = vec![];
/// for i in 0..num_elements {
///     input.push(i);
/// }
///
/// let output = sum(&input);
/// ```
pub fn sum(input: &Vec<i32>) -> i32 {
    let mut output: i32 = 0;
    unsafe {
        output = cuda_sum(input.as_ptr(), input.len() as usize);
    }
    output
}

/// Performs FFT of input.
///
/// # Arguments
///
/// * `input` - Input vector to be transformed.
///
/// # Examples
///
/// ```
/// use fftserver::single_fft;
/// use num_complex::Complex;
///
/// let num_elements = 1000;
/// let fc = 250.0;
/// let fs = 1_000.0;
/// let mut t = vec![];
/// for i in 0..num_elements {
///     t.push(i as f32 / fs);
/// }
///
/// let input = t.iter().map(|x| Complex::new((std::f32::consts::PI * 2.0 * fc * x).cos(), 0.0)).collect();
/// let output = single_fft(&input);
/// ```
pub fn single_fft(input: &Vec<Complex<f32>>) -> Vec<Complex<f32>> {
    let mut output = Vec::with_capacity(input.len());
    unsafe {
        cuda_fft(input.as_ptr(), output.as_mut_ptr(), input.len() as usize);
        output.set_len(input.len());
    }
    output
}

/// Performs FFT of each sub-vector in input.
///
/// # Arguments
///
/// * `input` - Vector of input vectors to be transformed.
///
/// # Examples
///
/// ```
/// use fftserver::batch_fft;
/// use num_complex::Complex;
///
/// let num_elements = 1000;
/// let num_ffts = 16;
/// let fc = 250.0;
/// let fs = 1_000.0;
///
/// let mut input = vec![];
/// for _ in 0..num_ffts {
/// let mut t = vec![];
/// for i in 0..num_elements {
///    t.push(i as f32 / fs);
/// }
///
/// let input_single = t.iter().map(|x| Complex::new((std::f32::consts::PI * 2.0 * fc * x).cos(), 0.0)).collect();
///     input.push(input_single);
/// }
/// let output = batch_fft(&input);
/// ```
pub fn batch_fft(input: &Vec<Vec<Complex<f32>>>) -> Vec<Vec<Complex<f32>>> {
    let num_ffts = input.len() as usize;
    let nfft = input.first().expect("Expected non-empty input vector").len();
    let input1d = input.clone().into_iter().flatten().collect::<Vec<Complex<f32>>>();
    let mut output1d = Vec::with_capacity(input1d.len());
    unsafe {
        cuda_fft_batch(input1d.as_ptr(), output1d.as_mut_ptr(), nfft, num_ffts);
        output1d.set_len(input1d.len());
    }
    let mut output = vec![];
    while !output1d.is_empty() {
        let temp = output1d.drain(..nfft).collect();
        output.push(temp);
    }
    output
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cuda_single_fft() {
        // Short single_fft() test
        let num_elements = 1000;
        let fc = 250.0;
        let fs = 1_000.0;
        let mut t = vec![];
        for i in 0..num_elements {
            t.push(i as f32 / fs);
        }

        let input = t.iter().map(|x| Complex::new((std::f32::consts::PI * 2.0 * fc * x).cos(), 0.0)).collect();
        let output = single_fft(&input);

        let epsilon = 0.1;
        for (i, value) in output.iter().enumerate() {
            if i == 250 || i == 750 {
                assert!((value.re - 500.0).abs() < epsilon);
            } else {
                assert!(value.re.abs() < epsilon);
            }
        }
    }

    #[test]
    fn test_cuda_long_single_fft() {
        // Long single_fft() test
        let num_elements = 1000000;
        let fc = 250.0;
        let fs = 1_000.0;
        let mut t = vec![];
        for i in 0..num_elements {
            t.push(i as f32 / fs);
        }

        let input = t.iter().map(|x| Complex::new((std::f32::consts::PI * 2.0 * fc * x).cos(), 0.0)).collect();
        let output = single_fft(&input);

        let epsilon = 5000.0;
        for (i, value) in output.iter().enumerate() {
            if i == 250000 || i == 750000 {
                assert!((value.re - 500000.0).abs() < epsilon);
            } else {
                assert!(value.re.abs() < epsilon);
            }
        }
    }

    #[test]
    fn test_cuda_batch_fft() {
        // Testing batch_fft() method
        let num_elements = 1000;
        let num_ffts = 16;
        let fc = 250.0;
        let fs = 1_000.0;

        let mut input = vec![];
        for _ in 0..num_ffts {
            let mut t = vec![];
            for i in 0..num_elements {
                t.push(i as f32 / fs);
            }

            let input_single = t.iter().map(|x| Complex::new((std::f32::consts::PI * 2.0 * fc * x).cos(), 0.0)).collect();
            input.push(input_single);
        }

        let output = batch_fft(&input);

        let epsilon = 0.1;
        for fft_result in output {
            for (i, value) in fft_result.iter().enumerate() {
                if i == 250 || i == 750 {
                    assert!((value.re - 500.0).abs() < epsilon);
                } else {
                    assert!(value.re.abs() < epsilon);
                }
            }
        }
    }

    #[test]
    fn test_cuda_sum() {
        let num_elements = 1 << 15;
        let mut input = vec![];
        for i in 0..num_elements {
            input.push(i);
        }

        let output = sum(&input);

        assert_eq!(output, (num_elements * (num_elements - 1) / 2) as i32);
    }
}
